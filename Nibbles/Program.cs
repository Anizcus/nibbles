﻿using System;
using System.Threading;

namespace Nibbles
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager scene = new Manager(Console.WindowWidth, Console.WindowHeight);
            scene.SwitchScene(Manager.State.Menu);

            Console.CursorVisible = false;

            while (scene.Running)
            {
                scene.Input();
                scene.Update();
                scene.Render();
                Thread.Sleep(100);
            }
        }
    }
}
