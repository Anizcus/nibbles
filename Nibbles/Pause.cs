﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nibbles
{
    public class Pause : Scene
    {
        private readonly string _pause;

        public Pause(Manager manager)
        {
            _manager = manager;
            _allowClear = false;
            _pause = "GAME PAUSED!";
        }

        public override void Input(ConsoleKey key)
        {
            if(ConsoleKey.P == key)
            {
                _manager.Pop();
                Console.Clear();
            }
        }

        public override void Render()
        {
            TextOnCenter(_pause, ConsoleColor.DarkMagenta, -3);
        }

        public override void Update()
        {
            
        }
    }
}
