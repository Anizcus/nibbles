﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nibbles
{
    class About : Scene
    {
        private readonly string[] description;

        public About(Manager manager)
        {
            _manager = manager;

            description = new string[6];

            description[0] = "This is simple console 'Nibbles' game also known as Snake game.";
            description[1] = "[ESC] Exits the game.";
            description[2] = "[M] in Game Over Screen returns you to main menu. (here too)";
            description[3] = "[Right, Left, Up, Down] Arrow moves the snake.";
            description[4] = "Snake extends its body as you eat blue circles.";
            description[5] = "Good Luck! :D";
        }

        public override void Input(ConsoleKey key)
        {
            if(ConsoleKey.M == key)
            {
                _manager.SwitchScene(Manager.State.Menu);
            }
        }

        public override void Render()
        {
            for (int i = 0; i < description.Count(); i++)
            {
                TextOnCenter(description[i], ConsoleColor.Gray, -3 + i);
            }
        }

        public override void Update()
        {

        }
    }
}
