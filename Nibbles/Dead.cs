﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nibbles
{
    public class Dead : Scene
    {
        public Dead(Manager manager)
        {
            _manager = manager;
            _allowClear = false;
        }

        public override void Input(ConsoleKey key)
        {
            if (ConsoleKey.Enter == key)
            {
                _manager.SwitchScene(Manager.State.Game);
                Console.Clear();
            } else if (ConsoleKey.M == key)
            {
                _manager.SwitchScene(Manager.State.Menu);
                Console.Clear();
            }
        }

        public override void Render()
        {
            TextOnCenter("Game Over!", ConsoleColor.Red, -3);
            TextOnCenter("Press [ESC] to exit or [Enter] to retry,", ConsoleColor.Gray, -1);
            TextOnCenter("[M] key will return you to main MENU.", ConsoleColor.Red, 0);
        }

        public override void Update()
        {
            
        }
    }
}
