﻿using System;
using System.Collections.Generic;

namespace Nibbles
{
    public class Game : Scene
    {
        private List<Pixel> wall;
        private Snake snake;
        private Pixel food;

        private int score;

        public Game(Manager manager)
        {
            _manager = manager;
            _allowClear = false;

            snake = new Snake(_manager.Height / 2 - 10);

            wall = new List<Pixel>();

            for (int i = 0; i < _manager.Width - 1; i++)
            {
                wall.Add(new Pixel(i, 0));
                wall.Add(new Pixel(i, _manager.Height - 10));
            }

            for (int i = 0; i < _manager.Height - 10; i++)
            {
                wall.Add(new Pixel(0, i));
                wall.Add(new Pixel(_manager.Width - 1, i));
            }

            for (int i = _manager.Width / 2 - 10; i < _manager.Width / 2 + 10; i++)
            {
                wall.Add(new Pixel(i, _manager.Height / 2 - 10));
                wall.Add(new Pixel(i, _manager.Height / 2));
            }

            generateFood();
            score = 0;
        }

        public override void Input(ConsoleKey key) {
            if(ConsoleKey.P == key)
            {
                _manager.Push(new Pause(_manager));
            } else
            {
                snake.Input(key);
            }
        }

        public override void Update()
        {
            snake.Update();

            if (snake.IsSelfBited() || WallPixelCollision(snake.Position))
            {
                _manager.SwitchScene(Manager.State.Dead);
            }

            if(snake.X == food.X && snake.Y == food.Y)
            {
                snake.Eat();
                generateFood();
                score++;
            }
        }

        public override void Render()
        {
            food.Render('O', ConsoleColor.Blue);
            snake.Render();
            foreach(var pixel in wall)
            {
                pixel.Render('#', ConsoleColor.White);
            }

            TextOnCenter(String.Format("Score: {0}", score), ConsoleColor.Cyan, 7);
        }

        private Pixel generateFood()
        {
            Random rand = new Random();
            food = new Pixel();

            do
            {
                food.X = rand.Next(1, _manager.Width - 2);
                food.Y = rand.Next(1, _manager.Height - 12);
            } while (WallPixelCollision(food));

            return food;
        }

        private bool WallPixelCollision(Pixel obj)
        {
            foreach (var pixel in wall)
            {
                if (pixel.X == obj.X && pixel.Y == obj.Y)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
