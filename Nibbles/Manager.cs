﻿using System;
using System.Collections.Generic;

namespace Nibbles
{
    public class Manager
    {
        private Stack<Scene> _Scene;

        public bool Running { get; set; }
        public enum State
        {
            Game,
            Menu,
            About,
            Dead
        }

        public int Width { get; private set; }
        public int Height { get; private set; }

        public Manager(int width, int height)
        {
            Width = width;
            Height = height;

            _Scene = new Stack<Scene>();

            Running = true;
        }

        public void Pop()
        {
            _Scene.Pop();
        }

        public void Push(Scene scene)
        {
            _Scene.Push(scene);
        }

        public void Input()
        {
            if (Console.KeyAvailable)
            {
                ConsoleKey key = Console.ReadKey().Key;
                if (ConsoleKey.Escape != key)
                {
                    _Scene.Peek().ClrScr();
                    _Scene.Peek().Input(key);
                }
                else
                {
                    Running = false;
                }
            }
        }

        public void Update()
        {
            _Scene.Peek().Update();
        }

        public void Render()
        {
            _Scene.Peek().Render();
        }

        public void SwitchScene(State state)
        {
            while (_Scene.Count > 0) { _Scene.Pop(); }
            switch (state)
            {
                case State.Game: _Scene.Push(new Game(this));
                    break;
                case State.Menu: _Scene.Push(new Menu(this));
                    break;
                case State.Dead: _Scene.Push(new Dead(this));
                    break;
                case State.About: _Scene.Push(new About(this));
                    break;
            }
        }
    }
}
