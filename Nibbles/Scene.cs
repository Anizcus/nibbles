﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nibbles
{
    public abstract class Scene
    {
        protected Manager _manager { get; set; }
        protected bool _allowClear = true;

        public abstract void Input(ConsoleKey key);
        public abstract void Update();
        public abstract void Render();

        public void TextOnCenter(string text, ConsoleColor color, int index)
        {
            Console.SetCursorPosition(
                _manager.Width / 2 - text.Length / 2,
                _manager.Height / 2 + index
            );
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.SetCursorPosition(0, 0);
        }

        public void ClrScr()
        {
            if (_allowClear)
            {
                Console.Clear();
            }
        }
    }
}
