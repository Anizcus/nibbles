﻿using System;
using System.Collections.Generic;

namespace Nibbles
{
    public class Snake
    {

        #region variables
            private int xDir;
            private int yDir;

            public int X {
                get { return _snake[0].X;  }
                private set { _snake[0].X = value; }
            }

            public int Y {
                get { return _snake[0].Y;  }
                private set { _snake[0].Y = value; }
            }

            public Pixel Position {
                get { return _snake[0]; }
            }

            private List<Pixel> _snake;
            private Pixel _trail;
        #endregion

        public Snake(int y)
        {
            _snake = new List<Pixel>()
            {
                new Pixel(3, y),
                new Pixel(2, y),
                new Pixel(1, y)
            };

            _trail = new Pixel()
            {
                X = _snake[_snake.Count - 1].X,
                Y = _snake[_snake.Count - 1].Y
            };

            yDir = 0;
            xDir = 0;
        }

        public void Input(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.DownArrow:
                    {
                        yDir = 1; xDir = 0; break;
                    }
                case ConsoleKey.UpArrow:
                    {
                        yDir = -1; xDir = 0; break;
                    }
                case ConsoleKey.LeftArrow:
                    {
                        xDir = -1; yDir = 0; break;
                    }
                case ConsoleKey.RightArrow:
                    {
                        xDir = 1; yDir = 0; break;
                        
                    }
            }
        }

        public void Update()
        {
            if (xDir != 0 || yDir != 0)
            {
                _trail.X = _snake[_snake.Count - 1].X;
                _trail.Y = _snake[_snake.Count - 1].Y;
                
                for (int i = _snake.Count - 1; i > 0; i--)
                {
                    _snake[i].X = _snake[i - 1].X;
                    _snake[i].Y = _snake[i - 1].Y;
                }

                X += xDir;
                Y += yDir;
            }
        }

        public void Eat()
        {
            _snake.Add(new Pixel(X, Y));
        }

        public void Render()
        {
            _trail.Render(' ', ConsoleColor.White);
            _snake[0].Render('O', ConsoleColor.Red);
            for(int i = 1; i < _snake.Count - 1; i++)
            {
                _snake[i].Render('O', ConsoleColor.Yellow);
            }
            _snake[_snake.Count - 1].Render('O', ConsoleColor.Green);
        }

        public bool IsSelfBited()
        {
            for(int i = 1; i < _snake.Count; i++)
            {
                if (_snake[0].X == _snake[i].X && _snake[0].Y == _snake[i].Y)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
