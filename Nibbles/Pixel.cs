﻿using System;

namespace Nibbles
{
    public class Pixel
    {
        private int _x;
        private int _y;

        public int X {
            get { return _x; }
            set { _x = value < 0 ? 0 : value; }
        }

        public int Y {
            get { return _y; }
            set { _y = value < 0 ? 0 : value; }
        }

        public Pixel()
        {
            X = 0;
            Y = 0;
        }

        public Pixel(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void Render(char c, ConsoleColor color)
        {
            Console.SetCursorPosition(X, Y);
            Console.ForegroundColor = color;
            Console.Write(c);
            Console.SetCursorPosition(0, 0);
        }
    }
}
