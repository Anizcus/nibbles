﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nibbles
{
    public class Menu : Scene
    {
        private readonly string[] choices;
        private int choice;

        public Menu(Manager manager)
        {
            _manager = manager;

            choices = new string[3];

            choices[0] = "Play";
            choices[1] = "Instructions";
            choices[2] = "Exit";

            choice = 0;
        }

        public override void Input(ConsoleKey key)
        {
            if(ConsoleKey.DownArrow == key)
            {
                choice++;
            } else if (ConsoleKey.UpArrow == key)
            {
                choice--;
            } else if (ConsoleKey.Enter == key)
            {
                switch (choice)
                {
                    case 0: _manager.SwitchScene(Manager.State.Game);
                        break;
                    case 1: _manager.SwitchScene(Manager.State.About);
                        break;
                    case 2: _manager.Running = false;
                        break;
                }
            }
            if (choice > 2)
            {
                choice = 0;
            }else if (choice < 0)
            {
                choice = 2;
            }
        }

        public override void Render()
        {
            for(int i = 0; i < choices.Count(); i++)
            {
                if(i == choice)
                {
                    TextOnCenter(choices[i], ConsoleColor.Red, -1 + i);
                }
                else
                {
                    TextOnCenter(choices[i], ConsoleColor.Gray, -1 + i);
                }
            }
        }

        public override void Update()
        {
            
        }
    }
}
